import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in); 
		Dog [] packOfDog = new Dog[1];
		
		for (int i = 0 ; i < packOfDog.length; i++){
	
			System.out.println("Give me the age for the dog");
            int dogAge = Integer.parseInt(reader.nextLine());
			// Set the age field of the Dog object
            
            System.out.println("Give a name for the dog");
            String dogName = reader.nextLine();
			// Set the name field of the Dog object

            System.out.println("Is the dog mixed? (true/false)");
			//Take the boolean value as a string
			String mixedInput = reader.nextLine();
			//Convert string value into a boolean
            Boolean dogBreed = Boolean.parseBoolean(mixedInput);
			
			packOfDog[i] = new Dog(dogAge, dogName, dogBreed);
			
			System.out.println(packOfDog[packOfDog.length -1].getAge());
			System.out.println(packOfDog[packOfDog.length -1].getName());
			System.out.println(packOfDog[packOfDog.length -1].getIsMixedBreed());
			
		}
		System.out.println("Give me the age for the dog");
		packOfDog[packOfDog.length -1].setAge(Integer.parseInt(reader.nextLine()));
		packOfDog[packOfDog.length -1].getAge();
		
		System.out.println(packOfDog[packOfDog.length -1].getAge());
		System.out.println(packOfDog[packOfDog.length -1].getName());
		System.out.println(packOfDog[packOfDog.length -1].getIsMixedBreed());
		
		
		
	}
}
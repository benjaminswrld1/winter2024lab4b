public class Dog{
	private int age;
	private String name;
	private boolean isMixedBreed;
	

	public String checkOffspringBreed(){
		if (age <=2){
			return name + " is still not ready to have kids";
		}
		else{
			if(isMixedBreed){
				return name + " can possibly share more than one breed type with it's child";
			}
			else{
				return name + " will only share one breed type with it's child";
			}
		}
		
	}
	public String play(){
		if (age < 4){
			return name + " is probably a really active dog";
		}
		else{
			return name + " is probably a less active dog";
		}
		
	}
	//Constructor
	public Dog(int age, String name, boolean isMixedBreed){
		this.age = age;
		this.name = name;
		this.isMixedBreed = isMixedBreed;
	}
	 
	// the three set methods
	public void setAge(int dogAge){
		this.age = dogAge;
	}
	
	//three get methods
	public int getAge(){
		return this.age;
	}
	public String getName(){
		return this.name;
	}
	public boolean getIsMixedBreed(){
		return this.isMixedBreed;
	}
}
